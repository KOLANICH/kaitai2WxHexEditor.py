kaitai2WxHexEditor.py [![Unlicensed work](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/)
===============
[![GitLab Build Status](https://gitlab.com/KOLANICH/kaitai2WxHexEditor.py/badges/master/pipeline.svg)](https://gitlab.com/KOLANICH/kaitai2WxHexEditor.py/pipelines/master/latest)
![GitLab Coverage](https://gitlab.com/KOLANICH/kaitai2WxHexEditor.py/badges/master/coverage.svg)
[![Coveralls Coverage](https://img.shields.io/coveralls/KOLANICH/kaitai2WxHexEditor.py.svg)](https://coveralls.io/r/KOLANICH/kaitai2WxHexEditor.py)
[![Libraries.io Status](https://img.shields.io/librariesio/github/KOLANICH/kaitai2WxHexEditor.py.svg)](https://libraries.io/github/KOLANICH/kaitai2WxHexEditor.py)

Generates tags files for WxHexEditor from results of parsing them by Kaitai Struct specs.

